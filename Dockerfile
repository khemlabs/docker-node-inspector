FROM node:4.2.1

EXPOSE 9080

RUN npm install -g node-inspector

CMD ["node-inspector", "--web-port", "9080"]
